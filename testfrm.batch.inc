<?php
/**
 * @file Batch.
 */

/**
 * Batch operations.
 * @param $nid - node id
 * @param $context
 */
function testfrm_batch_unpublish($limit, &$context) {

  $nids = testfrm_select_unpublish_node($limit)->fetchCol();

  if (empty($context['sandbox'])) {
    $res = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('type', 'my_article')
      ->condition('status', NODE_PUBLISHED)
      ->condition('created', strtotime('-1 day', REQUEST_TIME), '<');
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = $res->countQuery()->execute()->fetchField();
  }

  foreach ($nids as $nid) {
    $node = node_load($nid);
    $node->status = NODE_NOT_PUBLISHED;
    node_save($node);
    $context['sandbox']['progress']++;

    $context['message'] = t('Unpublished @count of @total posts', array(
      '@count' => $context['sandbox']['progress'],
      '@total' => $context['sandbox']['max'],
    ));

    $context['results'][] = $node->nid;

  }

  if (empty($nids)) {
    $context['finished'] = 1;
  }
  elseif ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }

}

/**
 * Function after batch finish.
 * @param $success
 * @param $results
 * @param $operations
 */
function testfrm_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('Unpublished @count posts', array('@count' => count($results))));
  }
  else {
    drupal_set_message(t('Finished with errors'), 'error');
  }
}