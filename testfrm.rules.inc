<?php
/**
 * @file Rules.
 */
 
/**
 * Implements hook_rules_action_info().
 */
/*function testfrm_rules_action_info() {
  $items['testfrm_get_nodes'] = array(
    'label' => t('Get nodes ID'),
    'group' => t('Testfrm'),
    'provides' => array(
      'nids' => array('type' => 'list<integer>', 'label' => t('Node IDs')),
    ),
  );
  return $items;
}*/

/**
 * Implements hook_rules_event_info().
 */
function testfrm_rules_event_info() {
  $items = array(
    /*'testfrm_event_10pm' => array(
      'label' => t('10pm event'),
      'group' => 'Testfrm',
    ),*/
    'testfrm_event_nid' => array(
      'label' => t('Event NID'),
      'group' => 'Testfrm',
      'module' => 'testfrm',
      /*'arguments' => array(
        'arg_node_id' => array('type' => 'integer', 'label' => t('Node ID')),
      ),*/
      'variables' => array(
        'var_node_id' => array('type' => 'integer', 'label' => t('Node ID variable.')),
      ),
    ),
  );
  return $items;
}
