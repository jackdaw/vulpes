<?php
/**
 * @file
 * Ctools content type.
 */

function testfrm_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'testfrm') . '/handlers',
    ),
    'handlers' => array(
      'views_handler_field_testfrm_timestamp' => array(
        'parent' => 'views_handler_field_numeric',
      ),
    ),
  );
}

/**
 * Implements hook_views_data().
 */
function testfrm_views_data() {

  $data['t_testfrm']['table']['base'] = array(
    'title' => t('Testfrm View'),
    'help' => t('Testfrm View'),
  );

  $data['t_testfrm']['id'] = array(
    'title' => t('ID'),
    'help'  => t('Message ID'),
    'group' => t('Testfrm'),
    'field' => array('handler' => 'views_handler_field_numeric'),
    'sort'  => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'views_handler_filter_numeric'),
  );

  $data['t_testfrm']['title'] = array(
    'title'  => t('Title of message'),
    'help'   => t('Title of message'),
    'group'  => t('Testfrm'),
    'field' => array('handler' => 'views_handler_field'),
    'sort' => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'views_handler_filter_string'),
  );

  $data['t_testfrm']['section'] = array(
    'title'  => t('Category'),
    'help'   => t('Category'),
    'group'  => t('Testfrm'),
    'field'  => array('handler' => 'views_handler_field_numeric'),
    'sort'   => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'views_handler_filter_numeric'),
  );

  $data['t_testfrm']['date_create'] = array(
    'title'  => t('Timestamp of message'),
    'help'   => t('Timestamp of message create'),
    'group'  => t('Testfrm'),
    'field'  => array('handler' => 'views_handler_field_testfrm_timestamp'),
    'sort'   => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'views_handler_filter_numeric'),
  );

  $data['t_testfrm']['descript'] = array(
    'title'  => t('Message'),
    'help'   => t('User\'s message'),
    'group'  => t('Testfrm'),
    'field'  => array('handler' => 'views_handler_field'),
    'sort'   => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'views_handler_filter_string'),
  );

  $data['t_testfrm']['fileid'] = array(
    'title'  => t('FileID'),
    'help'   => t('FileID'),
    'group'  => t('Testfrm'),
    'relationship' => array(
      'group' => t('File managed'),
      'base' => 'file_managed',
      'base field' => 'fid',
      'handler' => 'views_handler_relationship',
      'label' => t('Get the file props.'),
      'title' => t('Get files by FID'),
      'help' => t('More information on this relationship'),
    ),
    'field'  => array('handler' => 'views_handler_field_numeric'),
    'sort'   => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'views_handler_filter_numeric'),
  );

  return $data;
}

/**
 * Implements hook_views_pre_render().
 */
/*function testfrm_views_pre_render(&$view) {
  if ($view->name == 'testfrm') {
    foreach ($view->result as &$row) {
      $row->field_myfield[0]['rendered']['#markup'] = $row->field_myfield[0]['rendered']['#markup'] ? t('Yes') : t('No');
    }
  }
}*/